export declare enum VersionType {
    MAJOR = "MAJOR",
    MINOR = "MINOR",
    PATCH = "PATCH"
}
declare function upgrade(type: VersionType): Promise<void>;
export declare function upgradePath(type: VersionType, path: string): Promise<{
    found: string;
    replaced: string;
    error: boolean;
} | {
    found: number;
    replaced: number;
    error: any;
}>;
export declare function getVersionNumber(path: string): {
    version: string;
    data: string;
};
export declare function reset(path: string): {
    found: string;
    replaced: string;
    error: boolean;
} | {
    found: number;
    replaced: number;
    error: any;
};
export default upgrade;
