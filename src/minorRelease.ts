import upgrade, { VersionType } from './release';

upgrade(VersionType.MINOR);

process.exit();
