import fs from 'fs';

export enum VersionType {
	MAJOR = 'MAJOR',
	MINOR = 'MINOR',
	PATCH = 'PATCH',
}

const versions: VersionType[] = [VersionType.MAJOR, VersionType.MINOR, VersionType.PATCH];

const VERSION_REGEX = /[\d]+\.[\d]+\.[\d]/;

// When called from version_upgrader npm script, the package is on root instead of node_modules
const ROOT = __dirname.includes('node_modules')
	? __dirname.replace('node_modules/version-upgrader/dist', '')
	: __dirname.replace('dist', '');
const PACKAGE_FILE = `${ROOT}package.json`;
const HTML_FILE = `${ROOT}public/index.html`;

async function upgrade(type: VersionType) {
	upgradePath(type, PACKAGE_FILE);
	upgradePath(type, HTML_FILE);
	process.exit();
}

// This method is exported so it can be tested
export async function upgradePath(type: VersionType, path: string) {
	try {
		const { version, data } = getVersionNumber(path);
		const replacement = getReplacement(version, type);
		const replaced = data.replace(version, replacement);
		fs.writeFileSync(path, replaced);
		const { version: checkVersion } = getVersionNumber(path);
		if (checkVersion !== replacement) {
			throw `An error has ocurred, instead of replacing ${version} with ${replacement} was replaced with ${checkVersion}`;
		}
		log(path, `version ${version} updated with ${checkVersion}`);
		return { found: version, replaced, error: false };
	} catch (error) {
		if (error && error.message && error.message.includes('no such file or directory')) {
			log(path, '', 'Project without file');
		} else {
			log(path, '', error);
		}
		return { found: -1, replaced: -1, error };
	}
}

// This method is exported so it can be tested
export function getVersionNumber(path: string): { version: string; data: string } {
	let data = fs.readFileSync(path).toString('utf8');
	let VERSION_NUMBER = '';
	if (path.includes('.html')) {
		const HTML_DATA = data.substring(data.indexOf('<title>') + 7, data.indexOf('</title>'));
		VERSION_NUMBER = HTML_DATA;
	} else {
		VERSION_NUMBER = data;
	}
	const version = VERSION_NUMBER.match(VERSION_REGEX);
	if (!Array.isArray(version) || typeof version[0] !== 'string') {
		throw `No version number found`;
	}
	return { version: version[0], data };
}

function getReplacement(version: string, updateVersion: VersionType) {
	const versionArray = version.split('.').map((versionNumber, index) => {
		const versionType = getVersionType(index);
		// [MAYOR.MINOR.PATCH]
		switch (updateVersion) {
			case VersionType.PATCH:
				if (versionType === VersionType.PATCH) {
					return parseInt(versionNumber) + 1;
				}
				return versionNumber;
			case VersionType.MINOR:
				if (versionType === VersionType.MINOR) {
					return parseInt(versionNumber) + 1;
				}
				if (versionType === VersionType.PATCH) {
					return 0;
				}
				return versionNumber;
			case VersionType.MAJOR:
				if (versionType === VersionType.MAJOR) {
					return parseInt(versionNumber) + 1;
				}
				return 0;
		}
	});
	return versionArray.join('.');
}

function getVersionType(index: number): VersionType {
	if (!versions[index]) {
		throw `Index (${index}) is invalid for a VersionType`;
	}
	return versions[index]!;
}

function log(path: string, message: string, error?: any) {
	if (error) {
		console.warn(`\x1b[33mversion-upgrader (${path}): ${getErrorMessage(error)} \x1b[0m`);
		return;
	}
	console.info(`version-upgrader (${path}): ${message}`);
}

function getErrorMessage(error: any): string {
	if (!error) {
		return 'unknown error';
	}
	return typeof error === 'string' ? error : error.message || 'unknown error';
}

// This method is used for testing
export function reset(path: string) {
	try {
		const { version, data } = getVersionNumber(path);
		const replacement = '0.0.0';
		const replaced = data.replace(version, replacement);
		fs.writeFileSync(path, replaced);
		log(path, `version updated from ${version} to ${replacement}`);
		return { found: version, replaced, error: false };
	} catch (error) {
		if (error && error.message && error.message.includes('no such file or directory')) {
			log(path, '', 'Project without file');
		} else {
			log(path, '', error);
		}
		return { found: -1, replaced: -1, error };
	}
}

export default upgrade;
