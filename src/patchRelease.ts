import upgrade, { VersionType } from './release';

upgrade(VersionType.PATCH);

process.exit();
