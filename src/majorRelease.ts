import upgrade, { VersionType } from './release';

upgrade(VersionType.MAJOR);

process.exit();
