import 'mocha';
import { expect } from 'chai';
import { upgradePath, getVersionNumber, reset, VersionType } from '../src/release';

const ROOT = `${__dirname}/assets/`;
const PACKAGE_FILE = `${ROOT}test.json`;
const HTML_FILE = `${ROOT}test.html`;

const validate = (file: string, expectedVersion: string, done: Mocha.Done) => {
	const { version } = getVersionNumber(file);
	expect(version).equal(expectedVersion);
	done();
};

describe('Validate (SRC) JSON: ', () => {
	before(function () {
		reset(PACKAGE_FILE);
	});

	it('Should expect version to be 0.0.0: ', (done) => {
		validate(PACKAGE_FILE, '0.0.0', done);
	});

	it('Should increment patch version to 0.0.1: ', (done) => {
		upgradePath(VersionType.PATCH, PACKAGE_FILE);
		validate(PACKAGE_FILE, '0.0.1', done);
	});

	it('Should increment minor version to 0.1.0: ', (done) => {
		upgradePath(VersionType.MINOR, PACKAGE_FILE);
		validate(PACKAGE_FILE, '0.1.0', done);
	});

	it('Should increment major version to 1.0.0: ', (done) => {
		upgradePath(VersionType.MAJOR, PACKAGE_FILE);
		validate(PACKAGE_FILE, '1.0.0', done);
	});

	it('Should set version to 0.0.0: ', (done) => {
		reset(PACKAGE_FILE);
		validate(PACKAGE_FILE, '0.0.0', done);
	});
});

describe('Validate (SRC) HTML: ', () => {
	before(function () {
		reset(HTML_FILE);
	});

	it('Should expect version to be 0.0.0: ', (done) => {
		validate(HTML_FILE, '0.0.0', done);
	});

	it('Should increment patch version to 0.0.1: ', (done) => {
		upgradePath(VersionType.PATCH, HTML_FILE);
		validate(HTML_FILE, '0.0.1', done);
	});

	it('Should increment minor version to 0.1.0: ', (done) => {
		upgradePath(VersionType.MINOR, HTML_FILE);
		validate(HTML_FILE, '0.1.0', done);
	});

	it('Should increment major version to 1.0.0: ', (done) => {
		upgradePath(VersionType.MAJOR, HTML_FILE);
		validate(HTML_FILE, '1.0.0', done);
	});

	it('Should set version to 0.0.0: ', (done) => {
		reset(HTML_FILE);
		validate(HTML_FILE, '0.0.0', done);
	});
});
