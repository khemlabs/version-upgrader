# README

Upgrade the package version or react html index file version number

## USAGE

Update your package.json with:

```json
{
	"scripts": {
		"upgrade:patch": "node node_modules/version-upgrader/dist/patchRelease.js",
		"upgrade:minor": "node node_modules/version-upgrader/dist/minorRelease.js",
		"upgrade:major": "node node_modules/version-upgrader/dist/majorRelease.js"
	}
}
```

With modified JSON call:

- yarn **upgrade:patch** to update major.minor.**PATCH**
- yarn **upgrade:minor** to update major.**MINOR**.patch
- yarn **upgrade:patch** to update **MAJOR**.minor.patch

## Who do I talk to?

- [dnangelus](https://github.com/DNAngeluS) repo owner and admin
- [elgambet](https://github.com/elgambet) developer and admin
